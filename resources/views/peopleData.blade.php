<!DOCTYPE html>
<html>
<head>
    <title>People</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
</head>
<style>
    body{
        background: lightgrey
    }
</style>
<body>

<div class="container-fluid">
    <div class="jumbotron">
        <form method="POST" action="/add">
            <div class="card-header">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="">Insert Jedi Name</label>
                    <input type="text" class="form-control" name="name">
                </div>
                <div class="form-group">
                    <label for="">Insert Jedi Heigth</label>
                    <input type="text" class="form-control" name="heigth"> 
                </div>
                <div class="form-group">
                    <label for="">Insert Jedi Masse</label>
                    <input type="text" class="form-control" name="masse">
                </div>
                <div class="form-group">
                    <label for="">Insert Jedi Gender</label>
                    <input type="text" class="form-control" name="gender">
                </div>
            </div>
            <div class="modal-footer">
                <a href="/peoples" type="button" class="btn btn-secondary">Close</a>
                <button type="submit" class="btn btn-primary">Save Changes</button>
            </div>
        </form>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
</body>
</html>