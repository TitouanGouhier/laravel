<!DOCTYPE html>
<html>
<head>
    <title>People</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
</head>
<style>
    body{
        background: lightgrey
    }
</style>
<body>

<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            @if(count($errors) > 0)

            <div class="alert alert-rouge">
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            @if(\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
            </div>
            @endif
            
            <div class="row">
                <div class="col-md-12 text-right">
                    <a href="/insertdata" class="btn btn-success badge-pill" style="width: 80px"> ADD </a>
                </div>
            </div>
            <br>
            <table class="table table-dark table-hover table-bordered">
                <thead>
                    <tr>
                        <th scope="col"> ID </th>
                        <th scope="col"> Name </th>
                        <th scope="col"> Heigth </th>
                        <th scope="col"> Masse </th>
                        <th scope="col"> Gender </th>

                        <th class="text-right"> Actions </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($peoples as $people)
                    <tr>
                        <th>{{$people->id}}</th>
                        <th> {{$people->name}} </th>
                        <th>{{$people->heigth}} </th>
                        <th> {{$people->masse}} </th>
                        <th> {{$people->gender}} </th>
                        <td class="text-right"> 
                            <a href="/editdata/{{ $people->id }}" class="btn btn-primary badge-pill" style="width:80px">EDIT</a>
                            <a href="/delete/{{ $people->id }}" class="btn btn-danger badge-pill" style="width:80px">X</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>

</body>
</html>