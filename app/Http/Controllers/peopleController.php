<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\people;

class peopleController extends Controller
{
    public function index()
    {
        $peoples = people::all();
        return view('peopleForm',compact('peoples'));
    }

    public function display()
    {
        return view('peopleData');
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'heigth' => 'required',
            'masse' => 'required',
            'gender' => 'required',
        ]);

        $peoples = new people();

        $peoples->name = $request->input('name');
        $peoples->heigth = $request->input('heigth');
        $peoples->masse = $request->input('masse');
        $peoples->gender = $request->input('gender');

        $peoples->save();

        return redirect('/peoples')->with('success','Peoples Data Saved');
    }
    public function edit(people $people)
    {
        return view('peopleDit',compact('people'));
    }
    public function updatedata(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'heigth' => 'required',
            'masse' => 'required',
            'gender' => 'required',
        ]);

        $peoples = people::find($id);

        $peoples->name = $request->input('name');
        $peoples->heigth = $request->input('heigth');
        $peoples->masse = $request->input('masse');
        $peoples->gender = $request->input('gender');

        $peoples->save();

        return redirect('/peoples')->with('success','Peoples Data Saved');
    }

    public function deletedata($id)
    {
        $peoples = People::findorFail((int)$id);
        $peoples->delete();

        return redirect('peoples')->with('success','Student Data Deleted');
    }
}
