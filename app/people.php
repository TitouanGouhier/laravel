<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class people extends Model
{
    protected $table = 'peoples';
    public $timestamps = false;
    protected $fillable = ['name','heigth','masse','gender','specie_id','planet_id'];

}
