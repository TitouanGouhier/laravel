<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/peoples','peopleController@index');
Route::get('/insertdata','peopleController@display');
Route::post('/add','peopleController@store');
Route::get('/editdata/{people}','peopleController@edit');
Route::put('/update/{id}','peopleController@updatedata')->name('update');
Route::get('/delete/{id}','peopleController@deletedata');